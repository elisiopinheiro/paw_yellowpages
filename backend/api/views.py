from django.shortcuts import render
from rest_framework import viewsets
from .models import Company
from .serializers import CompanySerializer
from django_filters.rest_framework import DjangoFilterBackend
from django_filters import rest_framework as filters

class CompanyFilter(filters.FilterSet):    

    class Meta:
        model = Company
        fields = {
            'name': ['icontains'],
            'address': ['icontains'],
            'city': ['icontains']
        }
class CompanyView(viewsets.ModelViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = CompanyFilter
