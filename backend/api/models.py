from django.db import models

class Company(models.Model):
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=50)
    postcode = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    count = models.IntegerField()

    def __str__(self):
        return self.name