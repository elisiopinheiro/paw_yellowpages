import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | backoffice/company-create', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:backoffice/company-create');
    assert.ok(route);
  });
});
