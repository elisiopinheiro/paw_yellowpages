import Route from '@ember/routing/route';

export default Route.extend({
    controllerName: 'backoffice/index',
    model(){
        return this.store.findAll('company')
    }
});
