import DS from 'ember-data';
const { 
    Model,
    attr 
} = DS;

export default Model.extend({
    name: attr('string'),
    address: attr('string'),
    postcode: attr('string'),
    city: attr('string'),
    count: attr('number')
});
