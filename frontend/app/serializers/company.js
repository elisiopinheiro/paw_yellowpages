import DS from 'ember-data';
import { merge } from '@ember/polyfills';

export default DS.RESTSerializer.extend({
    normalizeResponse(store, primaryModelClass, payload, id, requestType){        
        payload = {company: payload};
        return this._super(store, primaryModelClass, payload, id, requestType);
    },

    serializeIntoHash: function(hash, type, record, options) {
        merge(hash, this.serialize(record, options));
      }

});
