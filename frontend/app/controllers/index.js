import Controller from "@ember/controller";

export default Controller.extend({
  
  actions: {
    /*createCompany() {
      // get the input value from the .hbs template
      let newCompany = this.get("newCompany");
      let newAddress = this.get("newAddress");
      let newPostcode = this.get("newPostcode");
      let newCity = this.get("newCity");
      // create a record in Ember Data (locally, would not survive page refresh)
      let newRecord = this.store.createRecord("company", {
        name: newCompany,
        address: newAddress,
        postcode: newPostcode,
        city: newCity
      });
      // Save the record to the API endpoint specified in adapters/application.js
      newRecord.save();
    }
    readBoardGame() {
      // get the input value from the .hbs template
      //let id = this.get("boardGameId");
      // find the record (cheating and using id 1 from my mocked server)
      this.store.findRecord("company", 1).then(game => {
        alert(game.get("name") + " " + game.get("id"));
      });
    },
    updateBoardGame() {
      let updatedTitle = this.get("updatedTitle");
      let game = this.get("model").findBy("id", "1");
      game.set("title", updatedTitle); // locally update the title
      game.save(); // save the title to API via PATCH
    },
    destroyBoardGame() {
      let destroyId = this.get("destroyId");
      let game = this.get("model").findBy("id", destroyId);
      game.destroyRecord(); // destroy deletes & saves in one step
    },*/

    updateCount(id, counter) { 
      let game = this.get("model").findBy("id", id);
      game.set("count", counter+1); // locally update the title
      game.save(); // save the title to API via PATCH    
      
    },

    filterByCity(param) {
      if (param !== "") {
        return this.store.query("company", { city__icontains: param });
      } else {
        return this.store.findAll("company");
      }
    }  
  }
});
