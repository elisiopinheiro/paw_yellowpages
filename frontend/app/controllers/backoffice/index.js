import Controller from "@ember/controller";

export default Controller.extend({
  actions: {    
    destroyCompany(id) {      
      let comp = this.get("model").findBy("id", id);
      if (confirm("Are you sure?")) {
        comp.destroyRecord(); // destroy deletes & saves in one step
     }
      
    }
  }
});
