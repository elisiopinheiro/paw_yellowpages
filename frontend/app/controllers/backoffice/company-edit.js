import Controller from "@ember/controller";

export default Controller.extend({
  actions: {
    updateCompany() {
      let updatedName = this.get("model.name");
      let updatedAddress = this.get("model.address");
      let updatedPostcode = this.get("model.postcode");
      let updatedCity = this.get("model.city");
      let company = this.get("model");
      company.set("name", updatedName); // locally update the title
      company.set("address", updatedAddress);
      company.set("postcode", updatedPostcode);
      company.set("city", updatedCity);
      company.set("count", 32);
      company.save(); // save the title to API via PUT
    }
  }
});
