import Controller from "@ember/controller";

export default Controller.extend({
  actions: {
    createCompany() {
      // get the input value from the .hbs template
      let newCompany = this.get("newCompany");
      let newAddress = this.get("newAddress");
      let newPostcode = this.get("newPostcode");
      let newCity = this.get("newCity");
      // create a record in Ember Data (locally, would not survive page refresh)
      let newRecord = this.store.createRecord("company", {
          name: newCompany,
          address: newAddress,
          postcode: newPostcode,
          city: newCity,
          count: 0
        });
      // Save the record to the API endpoint specified in adapters/application.js      
      newRecord.save();
      this.transitionToRoute("backoffice.index");
    }
  }
});
