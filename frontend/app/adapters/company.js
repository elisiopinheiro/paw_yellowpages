import DS from "ember-data";

export default DS.RESTAdapter.extend({
  host: "http://localhost:8000",

  pathForType() {
    return "companies";
  },

  buildURL: function(type, id, record) {
    //call the default buildURL and then append a slash
    return this._super(type, id, record) + "/";
  }
});
