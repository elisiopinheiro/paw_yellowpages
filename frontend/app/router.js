import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('backoffice', function() {
    this.route('company-edit', { path: 'edit/:id' });
    this.route('company-create');
  });
  this.route('company-details', { path: 'company/:id' });
});

export default Router;
