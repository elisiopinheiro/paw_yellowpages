# How to run

[1] - open /frontend folder and run (ONLY FOR THE FIRST TIME):

``npm install``

[2] - go back to root folder and run:

``docker-compose up -d``

# Enter container shell commands

``docker-compose exec frontend sh``
``docker-compose exec backend sh``